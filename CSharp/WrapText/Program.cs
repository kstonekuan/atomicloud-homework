﻿using System;
using System.IO;

namespace WrapText
{
    class Program
    {
        static void Main(string[] args)
        {
            string wrapText(string original)
            {
                var splitted = original.Split(' ');
                var count = 0;
                var wrapped = "";
                foreach (var word in splitted)
                {
                    if (count < int.Parse(args[1]))
                    {
                        wrapped += word + " ";
                        count++;
                    }
                    else
                    {
                        wrapped.Remove(wrapped.Length - 1);
                        wrapped += '\n' + word + ' ';
                        count = 1;
                    }

                    wrapped.Remove(wrapped.Length - 1);
                }

                return wrapped;
            }

            File.WriteAllText(args[0], wrapText(File.ReadAllText(args[0])));
        }
    }
}

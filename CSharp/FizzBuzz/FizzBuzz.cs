﻿using System.Collections.Generic;

namespace FizzBuzz
{
    public class FizzBuzz
    {
        public string X { get; set; }
        
        public string Y { get; set; }

        public FizzBuzz(int size, IReadOnlyList<string> arr)
        {
            X = size > 1 ? arr[1] : "Fizz";
            Y = size > 2 ? arr[2] : "Buzz";
        }

        public string Output(int input)
        {
            var output = "";
            
            if (input % 3 == 0) output += X;
            if (input % 5 == 0) output += Y;
            if (input % 5 != 0 && input % 3 != 0) output += input;

            return output;
        }
        
    }
}
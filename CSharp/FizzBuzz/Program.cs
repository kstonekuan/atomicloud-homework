﻿using System;
using System.IO;
using System.Linq;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            var fb = new FizzBuzz(args.Length, args);

            var input = File.ReadAllText(args[0]);
            var ranges = input.Split('\n');

            foreach (var range in ranges)
            {
                var arr = range.Split(',').Select(int.Parse).ToList();
                var output = "";

                for (var i = arr[0]; i <= arr[1]; i++)
                {
                    output += fb.Output(i) + ' ';
                }

                output.Remove(output.Length - 1);
                Console.WriteLine(output);
            }
        }
    }
}

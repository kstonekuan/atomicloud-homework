# AtomiCloud Homework

This repository contains projects used as homework for AtomiCloud's Humble Programmer workshop. They were completed in Typescript using NodeJS and C# using .NET sdk.

## Getting Started

### Prerequisites

- Node.js v13.2.0
- Yarn v1.19.2
- ts-node v8.5.4
- CyanPrint v0.13.8
- .NET Core 3.0 SDK (dotnet v3.0.101)

### Installation

Use `npm install` inside HelloTypescript and NaiveDatabase folders to install required node modules.

## Deployment

To deploy on DigitalOcean droplet, the following was added to appsettings.json in HelloASP:

```
"Kestrel": {
    "EndPoints": {
      "Http": {
        "Url": "http://0.0.0.0:80"
      }
    }
  },
```

You can then curl using the IP address of the droplet on port 80

![deployment test](./digital-ocean-curl.png "Deployment on DigitalOcean")

## Testing

### HelloTypscript

HTTP Server

```bash
ts-node hello.ts
```

WrapText

```bash
ts-node wrap.ts text.txt 3
```

FizzBuzz

```bash
ts-node fizzbuzz.ts config.txt Fuzzy Wuzzy
```

### CSharp

WrapText

```bash
dotnet run -- text.txt 3
```

FizzBuzz

```bash
dotnet run -- config.txt Fuzzy Wuzzy
```

### HelloASP

```bash
dotnet run
```

```bash
curl http://localhost:5000/api/x5 --request POST --data "5" -H "Content-Type: application/json"
```

### NaiveDatabase

```bash
ts-node db.ts save name jerry
```

```bash
ts-node db.ts load name
```

```bash
ts-node db.ts delete name
```
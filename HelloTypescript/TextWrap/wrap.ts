import * as fs from "fs";

// This function reads the string from the file.
let readText = (filename: string): string => {
    return fs.readFileSync(filename, 'utf8');
};

// This function takes the string and the word limit to wrap the text into separate lines.
let wrapText = (str: string, wordLimit: number): string => {
    let words: Array<string> = str.split(' ');
    let count: number = 0, wrapped: string = "";

    for (const word of words) {
        if (count++ < wordLimit) {
            wrapped += word + " ";
        } else {
            count = 1;
            wrapped = wrapped.slice(0, -1) + "\n" + word + " ";
        }
    }

    wrapped = (wrapped.slice(0, -1));

    return wrapped;
};

// This function writes the wrapped text back into the file.
let writeText = (filename: string, str: string): void => {
        fs.writeFileSync(filename, str);
};

// Get the file name and word limit.
let args: Array<string> = process.argv.splice(2);

// Process the file.
writeText(args[0], wrapText(readText(args[0]), parseInt(args[1])));
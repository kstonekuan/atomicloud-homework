import {Animal} from "./animal";

class Dog implements Animal {
    ChineseCry(): string {
        return "wang";
    }

    EnglishCry(): string {
        return "woof";
    }

    JapaneseCry(): string {
        return "wan";
    }

}

export {Dog}
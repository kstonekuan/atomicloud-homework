import express, {Request, Response} from "express"
import bodyParser from "body-parser";
import {SumOfDigits} from "./util";

const port = 3000;

const app = express();

app.use(bodyParser.text({type: "*/*"}));

app.get('/', (_: Request, res: Response) => {
    res.send('Hello World');
});

app.get('/zhang', (_: Request, res: Response) => {
    res.send('Zhang is indeed the best');
});

app.post('/sum', (req: Request, res: Response) => {
    const x = parseInt(req.body);
    res.send(SumOfDigits(x).toString());
});

app.listen(port, () => console.log(`App listening on port ${port}!`));
import * as fs from "fs";
import {FizzBuzz} from "./class"

// Get the file name and x and y.
let args: Array<string> = process.argv.splice(2), fb = new FizzBuzz();

// Replace values of x and y if they are provided.
if (args[1]) fb.x = args[1];
if (args[2]) fb.y = args[2];

let str: string = fs.readFileSync(args[0], 'utf8');

let input: Array<string> = str.split('\n');

for (const line of input) {
    // Get the range from each pair of numbers.
    let range: Array<number> = line.split(',').map(Number), output: string = "";
    // Add the desired string to the output.
    for (let i = range[0]; i <= range[1]; i++) {
        if (i % 3 === 0) output += fb.x;
        if (i % 5 === 0) output += fb.y;
        if (i % 3 && i % 5) output += i;
        output += " ";
    }
    console.log(output);
}
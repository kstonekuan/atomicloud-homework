class FizzBuzz {
    constructor(public x: string = "Fizz", public y: string = "Buzz") {}
}

export {FizzBuzz}
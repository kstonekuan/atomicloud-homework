import * as ut from "./util";

let filename: string = "data.txt";

// Read the existing data
let data: string[][] = ut.readData(filename);

// Get the command, key and value
let args: Array<string> = process.argv.splice(2);

// Include any extra arguments as part of the value
if (args.length > 3) {
    let i: number = 3;
    while(args[i]) args[2] += " " + args[i++];
}

// Decide which function to carry out based on the command
switch (args[0]) {
    case "save":
        ut.save(args[1], args[2], data);
        break;
    case "load":
        console.log(ut.load(args[1], data));
        break;
    case "delete":
        data = ut.del(args[1], data);
        break;
    default:
        console.log("Command not found");
}

// Write back to the file
ut.writeData(filename, data);
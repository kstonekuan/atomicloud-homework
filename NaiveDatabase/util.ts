import * as fs from "fs";

// This function adds a new key value pair or modifies an existing one
let save = (key: string, value: string, data: string[][]): void => {
    let changed: number = 0;

    for (const entry of data) {
        if (entry[0] === key) {
            entry[1] = value;
            changed = 1;
        }
    }

    if (!changed) data.push([key, value]);
};

// This function returns the value from a given key
let load = (key: string, data: string[][]): string => {
    for (const entry of data) {
        if (entry[0] === key) {
            return entry[1];
        }
    }

    return "NULL";
};

// This function removes a key value pair.
let del = (key: string, data: string[][]): string[][] => {
    return data.filter((value) => value[0] != key);
};

// This function reads in the data and formats it into a 2D array
let readData = (filename: string): string[][] => {
    let input: string = fs.readFileSync(filename, "utf-8");
    let lines: Array<string> = input.split('\n');

    let data: string[][] = [];
    let i: number = 0;
    for (const line of lines) {
        data[i++] = line.split(',');
    }

    return data;
};

// This function formats the 2D data array into a string to write into the file
let writeData = (filename: string, data: string[][]) => {
    let output = "";

        for (const entry of data) {
            if (entry[0] != "") output += entry[0] + "," + entry[1] + "\n";
        }
        output = output.slice(0, -1);

    fs.writeFileSync(filename, output);
};

export {save, load, del, readData, writeData}